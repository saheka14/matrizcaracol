package software.caracol.matriz;

public class MatrizCaracol {

    public MatrizCaracol(){}

    /**
     * Genera una matriz bidimencional de caracol en base a las fila y columnas recibidas
     * @param filas cantidad de fila de la matriz
     * @param columnas cantidad de columnas de la matriz
     * @return matriz bidimencional de caracol
     */
    public int [][] GenerarMatriz(int filas, int columnas)
    {
        int[][] matrizCaracol = new int[filas][columnas];

        try {

            int countRight = columnas - 1;
            int countDown = filas - 1;
            int countLeft = 0;
            int countUp = 0;
            int punteroFila = 0;
            int punteroColumnas = 0;

            String direccion = "right";

            for (int i = 1; i <= filas * columnas; i++) {
                matrizCaracol[punteroFila][punteroColumnas] = i;
                switch (direccion) {
                    case "right":
                        if (punteroColumnas == countRight) {
                            direccion = "down";
                            punteroFila++;
                        } else {
                            punteroColumnas++;
                        }
                        break;
                    case "down":
                        if (punteroFila == countDown) {
                            direccion = "left";
                            punteroColumnas--;
                        } else {
                            punteroFila++;
                        }
                        break;
                    case "left":
                        if (punteroColumnas == countLeft) {
                            direccion = "up";
                            punteroFila--;
                            countUp++;
                        } else {
                            punteroColumnas--;
                        }
                        break;
                    case "up":
                        if (punteroFila == countUp) {
                            direccion = "right";
                            countRight--;
                            countDown--;
                            countLeft++;
                            punteroColumnas++;
                        } else {
                            punteroFila--;
                        }
                        break;
                }
            }
        }catch(Exception ex){
            System.out.println("Error al generar matriz de caracol: " + ex.getMessage());
        }
        return matrizCaracol;
    }
    /**
     * Recibe una matriz bidimencional de caracol y la convierte en un arreglo unidimencional
     * @param matrizCaracol es la matriz bidimencional de caracol
     * @return arreglo unidimencional
     */
    public int [] ConvertirArregloUnidimenacional(int [][] matrizCaracol)
    {
        int filas = matrizCaracol != null ? matrizCaracol.length : 0;
        int columnas =  matrizCaracol != null ? matrizCaracol[0].length : 0;
        int[] unidimencional = new int[filas * columnas];

        try {

            int countRight = columnas - 1;
            int countDown = filas - 1;
            int countLeft = 0;
            int countUp = 0;
            int punteroFila = 0;
            int punteroColumnas = 0;


            String direccion = "right";

            for (int i = 0; i < filas * columnas; i++) {

                unidimencional[i] = matrizCaracol[punteroFila][punteroColumnas];

                switch (direccion) {

                    case "right":
                        if (punteroColumnas == countRight) {
                            direccion = "down";
                            punteroFila++;
                        } else {
                            punteroColumnas++;
                        }
                        break;

                    case "down":
                        if (punteroFila == countDown) {
                            direccion = "left";
                            punteroColumnas--;
                        } else {
                            punteroFila++;
                        }
                        break;
                    case "left":
                        if (punteroColumnas == countLeft) {
                            direccion = "up";
                            punteroFila--;
                            countUp++;
                        } else {
                            punteroColumnas--;
                        }
                        break;
                    case "up":
                        if (punteroFila == countUp) {
                            direccion = "right";
                            countRight--;
                            countDown--;
                            countLeft++;
                            punteroColumnas++;
                        } else {
                            punteroFila--;
                        }
                        break;
                }
            }
        }catch(Exception ex){
            System.out.println("Error al convertir a unidimenacional: " + ex.getMessage());
        }
        return unidimencional;
    }
    /**
     * Imprime los elemento de una matriz bidimencional
     * @param matrizCaracol
     */
    public void ImprirmirBidimencional(int [][] matrizCaracol)
    {
        if(matrizCaracol != null)
        {
            for (int i = 0; i < matrizCaracol.length; i++) {
                for (int j = 0; j < matrizCaracol[0].length; j++) {
                    System.out.print(matrizCaracol[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }
    /**
     * Imprime los elemento de una matriz unidimenacional
     * @param unidimencional
     */
    public void ImprirmirUnidimencional(int [] unidimencional)
    {
        if(unidimencional!=null )
        {
            for (int i = 0; i < unidimencional.length; i++) {
                System.out.print(unidimencional[i] + " ");
            }
            System.out.println("");
        }
    }
}
