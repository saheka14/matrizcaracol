package software.caracol;

import software.caracol.matriz.MatrizCaracol;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("** Matriz de caracol **");
        Scanner sc = new Scanner(System.in);
        System.out.println("Filas:");
        int filas = sc.nextInt();
        System.out.println("Columnas:");
        int columnas = sc.nextInt();

        MatrizCaracol matrizCaracol = new MatrizCaracol();
        int [][] matrizBidimencional = matrizCaracol.GenerarMatriz(filas,columnas);
        int [] arregloUnidimencional = matrizCaracol.ConvertirArregloUnidimenacional(matrizBidimencional);

        System.out.println("Matriz Caracol:");
        matrizCaracol.ImprirmirBidimencional(matrizBidimencional);
        System.out.println("Arreglo Bidimencional:");
        matrizCaracol.ImprirmirUnidimencional(arregloUnidimencional);

    }
}
