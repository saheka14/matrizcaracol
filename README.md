# README #

La aplicación esta desarrollada en Java, mediante INTELLIJ IDEA Community Edition.

### What is this repository for? ###

* Quick summary
* Version 1.1

### Configuracion ###

* Dependencies: JDK 8
* Como ejectuar test: Instalar JRE 8 y ejecutar JAR
* Deployment instructions: 

La clase MatrizCaracol.java del paquete software.caracol.matriz, contiene los siguientes métodos:

##1. GenerarMatriz(int filas, int columnas)

     * Genera una matríz bidimencional de caracol en base a las fila y columnas recibidas
     * @param filas cantidad de fila de la matriz
     * @param columnas cantidad de columnas de la matriz
     * @return matriz bidimencional de caracol

##2. ConvertirArregloUnidimenacional(int [][] matrizCaracol)

	 * Recibe una matríz bidimencional de caracol y la convierte en un arreglo unidimencional
     * @param matrizCaracol es la matríz bidimencional de caracol
     * @return arreglo unidimencional

##3. ImprirmirBidimencional(int [][] matrizCaracol)
	 
	 * Imprime los elementos de una matríz bidimencional
     * @param matrizCaracol

##4. ImprirmirUnidimencional(int [] unidimencional)

     * Imprime los elementos de una matríz unidimenacional
     * @param unidimencional


### EJECUCION JAR EN CONSOLA ###

* Es posible ejecutar la aplicacion mediante el JAR incluido en el directorio SoftwareCaracol\out\artifacts\SoftwareCaracol_jar
* La ejecución es mediante consola en el directorio raiz del JAR mediente la sentencia java - jar SoftwareCaracol.jar
* Es necesario tener instalado la version 8 del JRE de Java